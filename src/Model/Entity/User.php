<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Validation\Validator;

/**
 * User Entity
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property int $gender
 * @property \Cake\I18n\FrozenDate $birthday
 * @property string $phone
 * @property string $username
 * @property string $password
 * @property string $email
 * @property int $role
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Booking[] $bookings
 */
class User extends Entity
{
    const DEFAULT_ADMIN_ID = 4;

    const STATUS_ACTIVE  = 1;
    const STATUS_BLOCKED = 2;

    const ROLE_ADMINISTRATOR = 1;
    const ROLE_EDITOR        = 2;

    const ROLE_PARAMS = [
        self::ROLE_ADMINISTRATOR => 'Administrator',
        self::ROLE_EDITOR        => 'Editor'
    ];
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'last_name' => true,
        'first_name' => true,
        'gender' => true,
        'birthday' => true,
        'phone' => true,
        'username' => true,
        'password' => true,
        'email' => true,
        'role' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'bookings' => true
    ];

    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', ['admin', 'author']],
                'message' => 'Please enter a valid role'
            ]);
    }
    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
