<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\ctlad;

use Cake\Controller\Controller;
use Cake\Event\Event;
use App\Controller\AppController;
use App\Model\Entity\User;

class LoginAppController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],

            'unauthorizedRedirect' => $this->referer()

]);

// Allow the display action so our pages controller
// continues to work.
$this->Auth->allow(['display']);
}
    /*public function beforeFilter(Event $event)
    {
        $this->set('userData', $this->user);
        return parent::beforeFilter($event);
    }*/
    public function isAuthorized($user)
    {
        // Admin can access every action
        if (isset($this->user) && $this->user['role'] === User::ROLE_ADMINISTRATOR) {
            return true;}

        // Default deny
        return false;
    }

}
