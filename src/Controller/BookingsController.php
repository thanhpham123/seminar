<?php
namespace App\Controller;

use App\Controller\ctlad\LoginAppController;
use Cake\Event\Event;
use App\Utility\Utility;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Utility\Text;
use App\Controller\UsersController;



/**
 * Bookings Controller
 *
 * @property \App\Model\Table\BookingsTable $Bookings
 *
 * @method \App\Model\Entity\Booking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */

class BookingsController extends LoginAppController
{
    public function beforeFilter(Event $event)
    {
        $this->viewBuilder()->setLayout('admin');

        return parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $au=$this->Auth->user('role');
        $uid=$this->Auth->user('id');

        $bookings = $this->paginate($this->Bookings);

        $this->set(compact('bookings','au','uid'));
    }

    /**
     * View method
     *
     * @param string|null $id Booking id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $booking = $this->Bookings->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('booking', $booking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {


            $booking = $this->Bookings->newEntity();

            if ($this->request->is('post')) {
                $dataPost = $this->request->getData();
                $url_avatar = "";
                $booking->user_id=$this->Auth->user('id');
                if (!empty($dataPost['attachment']['name'])) {
                    $url_avatar = Utility::uploadImage($dataPost['attachment'],'Bookings/attachment');
                }

                $booking = $this->Bookings->patchEntity($booking, $dataPost);
                if (!empty($dataPost['attachment']['name'])) {
                    $booking->attachment = $url_avatar;
                }
               /* die($booking);*/
                if ($this->Bookings->save($booking)) {
                    $this->Flash->success(__('Tạo Mới thành công.'));
                    return $this->redirect(['action' => 'index']);
                }
            }
        $this->set(compact('booking'));
    }
    /**
     * Edit method
     *
     * @param string|null $id Booking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $booking = $this->Bookings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $booking = $this->Bookings->patchEntity($booking, $this->request->getData());
            if ($this->Bookings->save($booking)) {
                $this->Flash->success(__('Sửa Thành Công.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Lỗi Không Sửa Được, Vui lòng Thử Lại.'));
        }
        $users = $this->Bookings->Users->find('list', ['limit' => 200]);
        $this->set(compact('booking', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Booking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $booking = $this->Bookings->get($id);
        if ($this->Bookings->delete($booking)) {
            $this->Flash->success(__('The booking has been deleted.'));
        } else {
            $this->Flash->error(__('The booking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($user)
    {
        // All registered users can add articles
        // Prior to 3.4.0 $this->request->param('action') was used.
        if ($this->request->getParam('action') === 'add') {
            return true;
        }
        // The owner of an article can edit and delete it
        // Prior to 3.4.0 $this->request->param('action') was used.
        if (in_array($this->request->getParam('action'), ['edit', 'delete'])) {
            // Prior to 3.4.0 $this->request->params('pass.0')
            $bookingId = (int)$this->request->getParam('pass.0');
            if ($this->booking->isOwnedBy($bookingId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }
}
