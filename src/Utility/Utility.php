<?php
namespace App\Utility;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

class Utility
{
    const JPEG = 'image/jpeg';
    const PNG = 'image/png';
    const JPG = 'image/jpg';

    const ALL = [
        self::JPEG,
        self::PNG,
        self::JPG,
    ];
     static function uploadImage($infoFile, $folder = 'images', $old_img = null)
     {
         $filename = null;
         if ($infoFile['size'] != 0 || $infoFile['error'] != 0) {
             if (in_array($infoFile['type'], Utility::ALL)) {
                 $filename = basename($infoFile['name']);
                 $uploadFolder = WWW_ROOT . 'uploads' . DS . $folder;
                 if (!file_exists($uploadFolder)) {
                     mkdir($uploadFolder, 0777, true);
                 }

                 $filename = time() . rand(0, 100000) . '.' . pathinfo($filename, PATHINFO_EXTENSION);
                 $uploadPath = $uploadFolder . DS . $filename;
                 try {
                     move_uploaded_file($infoFile['tmp_name'], $uploadPath);
                 } catch (\Exception $ex) {
                     return $ex;
                 }
                 if (!empty($old_img)) {
                     $old_avatar = explode('uploads', $old_img);
                     if (file_exists(WWW_ROOT . 'uploads' . $old_avatar[1])) {
                         unlink(WWW_ROOT . 'uploads' . $old_avatar[1]);
                     }
                 }
             }
         }

         return $filename ? Configure::read('App.fullBaseUrl') . '/uploads/' . $folder . '/' . $filename : null;
     }
}