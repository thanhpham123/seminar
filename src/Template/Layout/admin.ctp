<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CHia Sẻ Kiến Thức</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <?=$this->html->css('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
    <!-- Font Awesome -->
    <?=$this->html->css('bower_components/font-awesome/css/font-awesome.min.css')?>
    <!-- Theme style -->
    <?=$this->html->css('dist/css/AdminLTE.min.css')?>
    <?=$this->html->css('material-design-iconic-font.min')?>
    <!--===============================================================================================-->
    <?=$this->html->css('util')?>
    <?=$this->html->css('main')?>
    <?=$this->html->css('cmlg')?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <?= $this->html->css('dist/css/skins/_all-skins.min.css')?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Left side column. contains the logo and sidebar -->
    <?=  $this->element('Admin/Header');  ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?=  $this->element('Admin/sidebar');  ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="/Pages/home"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->

        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
        <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
        reserved.
    </footer>
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?=$this->html->script('bootstrap-datetimepicker.min.js') ?>
<?= $this->html->script('jQuery-2.1.4.min')?>
<?=$this->html->script('jquery-ui')?>
<?=$this->html->script('bootstrap.min')?>
<?=$this->html->script('jquery-3.2.1.min.js')?>
<?=$this->html->script('main.js') ?>

<?=$this->html->script('bootstrap-timepicker.min.js') ?>


</body>
</html>
