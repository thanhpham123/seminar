<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
use Cake\View\Helper\HtmlHelper;
?>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?=$this->html->css('bower_components/bootstrap/dist/css/bootstrap.min.css') ?>
    <!-- Font Awesome -->
    <?=$this->html->css('bower_components/font-awesome/css/font-awesome.min.css')?>
    <!-- Theme style -->
    <?=$this->html->css('dist/css/AdminLTE.min.css')?>
    <?=$this->html->css('material-design-iconic-font.min')?>
    <!--===============================================================================================-->
    <?=$this->html->css('util')?>
    <?=$this->html->css('main')?>
    <?=$this->html->css('cmlg')?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <?= $this->html->css('dist/css/skins/_all-skins.min.css')?>


</head>

<body>

    <?= $this->Flash->render() ?>
    <?= $this->fetch('content') ?>

<footer>
    <?= $this->html->script('jQuery-2.1.4.min')?>
    <?=$this->html->script('jquery-ui')?>
    <?=$this->html->script('bootstrap.min')?>
    <?=$this->html->script('jquery-3.2.1.min.js')?>
    <?=$this->html->script('main.js') ?>
</footer>
</body>