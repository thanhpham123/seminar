<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="favicon.ico"/>
    <!--===============================================================================================-->
    <?=$this->html->css('bower_components/bootstrap/dist/css/bootstrap.min') ?>
    <!-- Font Awesome -->
    <?=$this->html->css('bower_components/font-awesome/css/font-awesome.min')?>
    <!--===============================================================================================-->
    <?=$this->html->css('material-design-iconic-font.min')?>
    <!--===============================================================================================-->
    <?=$this->html->css('util')?>
    <?=$this->html->css('main')?>
    <!--===============================================================================================-->
</head>
<body>
    <?= $this->Flash->render() ?>

        <?= $this->fetch('content') ?>
    </div>
    <footer>

        <!--===============================================================================================-->
        <?=$this->html->script('jquery-3.2.1.min.js')?>
        <?=$this->html->script('main.js') ?>
    </footer>
</body>
</html>
