<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
use App\Model\Entity\User;
?>

<div class="jumbotron">

    <h1>List User</h1>
    <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'add']) ?>"
       class="btn btn-success btn-sm"> Thêm mới</a>
    <table cellpadding="0" cellspacing="0"  class="table table-striped">
        <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
            <th scope="col"><?= $this->Paginator->sort('gender') ?></th>
            <th scope="col"><?= $this->Paginator->sort('birthday') ?></th>
            <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
            <th scope="col"><?= $this->Paginator->sort('username') ?></th>
            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
            <th scope="col"><?= $this->Paginator->sort('role') ?></th>
            <th scope="col"><?= $this->Paginator->sort('status') ?></th>
            <th scope="col"><?= $this->Paginator->sort('created') ?></th>
            <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
            <th scope="col" class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->last_name) ?></td>
                <td><?= h($user->first_name) ?></td>
                <td><?= $user->gender == 1 ? 'Nam' : 'Nữ'?></td>
                <td><?= h($user->birthday) ?></td>
                <td><?= h($user->phone) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= $user->id == User::DEFAULT_ADMIN_ID ? 'Super Administrator' : User::ROLE_PARAMS[$user->role] ?></td>
                <td class="text-center">
                    <span class="label <?= $user->status == User::STATUS_ACTIVE ? 'bg-primary' : 'bg-red'?>"><?= $user->status == User::STATUS_ACTIVE ? 'Hoạt động' : 'Đã khoá'?></span>
                </td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->modified) ?></td>
                <td class="actions">
                    <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'view', $user->id]) ?>" class="btn btn-info btn-custom margin-bottom-cus" data-toggle="tooltip" title="Chi tiết">
                        <i class="fa fa-eye"></i>
                    </a>
                    <?php if($au==User::ROLE_ADMINISTRATOR){?>
                    <?php if($user->id != User::DEFAULT_ADMIN_ID ){ ?>
                    <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'edit', $user->id]) ?>" class="btn btn-warning btn-custom" data-toggle="tooltip" title="Sửa">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <?= $this->Form->postLink(
                        $this->Html->tag('i', '', array('class' => 'fa fa-trash')),
                        ['action' => 'delete', $user->id],
                        [
                            'escape' => false,
                            'class' => 'btn btn-danger btn-custom',
                            'data-toggle' => 'tooltip',
                            'title' => 'Xoá',
                            'confirm' => __('Bạn có muốn Xoá tài khoản này #{0}?', $user->id)
                        ]
                    ) ?>
                    <?php  }}?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>

    </div>
</div>