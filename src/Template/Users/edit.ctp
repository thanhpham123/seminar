<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <div class="col-md-9 col-md-push-3">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Edit User') ?></legend>
            <?
            echo $this->Form->control('last_name',['class'=>'form-control'] );
            echo $this->Form->control('first_name',['class'=>'form-control']);
            echo $this->Form->control('birthday',['class'=>'form-control']);
            echo $this->Form->control('phone',['class'=>'form-control']);
            echo $this->Form->control('username',['class'=>'form-control']);
            echo $this->Form->control('email',['class'=>'form-control']);
            echo $this->Form->control('password',['class'=>'form-control']);
            ?>

            <label for="exampleFormControlSelect1">Giới Tính</label>
            <div class="radio">

                <label>
                    <input type="radio" name="gender"  value="1" checked>
                    NAM
                </label>
                <label>
                    <input type="radio" name="gender"  value="2">
                    Nữ
                </label>
            </div>

            <label for="exampleFormControlSelect1">Trạng thái</label>
            <div class="radio">
                <label>
                    <input type="radio" name="status"  value="1" checked>
                    Hoạt động
                </label>
                <label>
                    <input type="radio" name="status"  value="2">
                    Khóa
                </label>
            </div>
           <!-- <div class="form-group">
                <label>Trạng thái</label>
                <p style="margin-top: 10px">
                    <input type="checkbox" name="status" value="1" <?/*= ($user->isNew() or $user->status == 1) ? 'checked' : ''*/?> data-toggle="toggle" data-on="<i class='fa fa-check'></i> Đã kiểm duyệt" data-off="Chờ kiểm duyệt" data-onstyle="info" >
                </p>
            </div>-->
            <label for="exampleFormControlSelect1">chức vụ</label>
            <div class="radio">
                <label>
                    <input type="radio" name="role"  value="1" checked>
                    Admin
                </label>
                <label>
                    <input type="radio" name="role" value="2">
                    Admin2
                </label>
            </div>

            <button type="submit" class="btn btn-success">Add</button>
        </fieldset>
        <?= $this->Form->end() ?>

    </div>
</div>