<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="row">

    <?= $this->Form->create($user) ?>
    <div class="col-md-9 col-md-push-3">
        <h2>Đăng Ký</h2>
        <div class="form-group">
            <label for="exampleFormControlInput1">last_name</label>
            <input type="text" class="form-control" name="last_name">

            <label for="exampleFormControlSelect1">first_name</label>
            <input type="text" class="form-control" name="first_name">

            <label for="exampleFormControlSelect1">birthday</label>
            <input type="date" class="form-control" name="birthday">

            <label for="exampleFormControlSelect1">username</label>
            <input type="text" class="form-control" name="username">

            <label for="exampleFormControlInput1">Email</label>
            <input type="email" class="form-control" name="email" placeholder="name@example.com">

            <label for="exampleFormControlSelect1">password</label>
            <input type="password" class="form-control" name="password">

            <label for="exampleFormControlSelect1">phone</label>
            <input type="text" class="form-control" name="phone">

            <label for="exampleFormControlSelect1">Giới Tính</label>
            <div class="radio">

                <label>
                    <input type="radio" name="gender"  value="1" checked>
                    NAM
                </label>
                <label>
                    <input type="radio" name="gender"  value="2">
                    Nữ
                </label>
            </div>

            <label for="exampleFormControlSelect1">Trạng thái</label>
            <div class="radio">
                <label>
                    <input type="radio" name="status"  value="1" checked>
                    Hoạt động
                </label>
                <label>
                    <input type="radio" name="status"  value="2">
                    Khóa
                </label>
            </div>


                    <input type="hidden" name="role"  value="2">



            <button type="submit" class="btn btn-success">Add</button>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

