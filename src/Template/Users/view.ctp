<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
use App\Model\Entity\User;

?>

<div class="row">
    <div class="col-md-9 col-md-push-3">
        <h3><?= h($user->first_name) ?></h3>
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('Last Name') ?></th>
                <td><?= h($user->last_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('First Name') ?></th>
                <td><?= h($user->first_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Phone') ?></th>
                <td><?= h($user->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Username') ?></th>
                <td><?= h($user->username) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Email') ?></th>
                <td><?= h($user->email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Password') ?></th>
                <td><?= h($user->password) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td>#<?= $this->Number->format($user->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Gender') ?></th>
                <td><?= $user->gender == 1 ? 'Nam' : 'Nữ'?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td class="">
                    <span class="label <?= $user->status == User::STATUS_ACTIVE ? 'bg-primary' : 'bg-red'?>"><?= $user->status == User::STATUS_ACTIVE ? 'Hoạt động' : 'Đã khoá'?></span>
                </td>
            </tr>
            <tr>
                <th scope="row"><?= __('Role') ?></th>
                <td><?= $user->id == User::DEFAULT_ADMIN_ID ? 'Super Administrator' : User::ROLE_PARAMS[$user->role] ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Birthday') ?></th>
                <td><?= $user->birthday ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($user->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($user->modified) ?></td>
            </tr>
        </table>
        <div class="related">
            <h2 style="color:#e81058"><?= __('Related Bookings') ?></h2>
            <?php if (!empty($user->bookings)): ?>
                <table class="table table-striped">
                    <tr>
                        <th scope="col"><?= __('Id') ?></th>
                        <th scope="col"><?= __('User Id') ?></th>
                        <th scope="col"><?= __('Title') ?></th>
                        <th scope="col"><?= __('Description') ?></th>
                        <th scope="col"><?= __('date_time') ?></th>
                        <th scope="col"><?= __('Created') ?></th>
                        <th scope="col"><?= __('Modified') ?></th>

                    </tr>
                    <?php foreach ($user->bookings as $bookings): ?>
                        <tr>
                            <td><?= h($bookings->id) ?></td>
                            <td><?= h($bookings->user_id) ?></td>
                            <td><?= h($bookings->title) ?></td>
                            <td><?= h($bookings->description) ?></td>
                            <td><?= h($bookings->date_time) ?></td>
                            <td><?= h($bookings->created) ?></td>
                            <td><?= h($bookings->modified) ?></td>

                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php endif; ?>
        </div>
    </div>

</div>