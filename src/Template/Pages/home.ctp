<?php
/**ne
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Booking $bookings
 */
?>
<h2>Danh Sách Lịch Đặt</h2>
<div class="row text-center">
    <a href="<?= $this->Url->build(['controller' => 'Bookings', 'action' => 'add']) ?>" class="btn btn-warning btn-sm"><h3>Đăng Ký Lịch Mới</h3></a>
</div>
<?php foreach ($bookings as $booking):?>
    <div class="col-12 col-md-4 col-lg-4">
    <div class="single-rooms-area wow fadeInUp" data-wow-delay="100ms">

        <div class="single-rooms-area wow fadeInUp" data-wow-delay="500ms">
            <!-- Thumbnail -->
            <div class="bg-thumbnail bg-img" style="background-image: url(<?= $booking->avatar?$booking->avatar:'img/bg-img/9.jpg'?>);"></div>
            <div class="rooms-text">
                <div class="line"></div>
                <h4>Tiêu Đề :<?=h($booking->title); ?></h4>
                <h4>Nội Dung :</h4><p><?=h($booking->description);?></p>
            </div>

            <a href="/Bookings/view/<?php echo $booking->id?>" class="btn btn-info btn-custom">Xem Chi Tiet</a>
        </div>
    </div>
    </div>
<?php endforeach; ?>


