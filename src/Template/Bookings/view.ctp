<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Booking $booking
 */
?>

<div class="bookings view large-9 medium-8 columns content">
    <h3><?= h($booking->title) ?></h3>
    <table class="table table-striped">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $booking->has('user') ? $this->Html->link($booking->user->first_name, ['controller' => 'Users', 'action' => 'view', $booking->user->first_name]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($booking->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($booking->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td> <?= $this->Text->autoParagraph(h($booking->description)); ?></td>
        </tr>
        <tr>
            <th><?= __('Attachment') ?></th>
            <td>  <?= $this->Text->autoParagraph(h($booking->attachment)); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($booking->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($booking->modified) ?></td>
        </tr>
    </table>

</div>
