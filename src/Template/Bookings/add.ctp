<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Booking $booking

 */
?>
<div class="col-12 col-md-6 col-lg-4">
    <div class="single-rooms-area wow fadeInUp" data-wow-delay="100ms">
        <?= $this->Form->create($booking) ?>
        <legend><?= __('Thêm Mới Lịch Trình Bày') ?></legend>
       <!-- <input type="number" name="user_id" value="<?php /*echo $newData */?>">-->

        <label for=""> tiêu đề</label>
        <input type="text" class="form-control" name="title">

        <?php
        echo $this->Form->control('description',['class'=>'form-control' ]);
        ?>


       <!-- <input type="datetime-local" class="form-control" name="date_time">-->

        <label for="image-link">Tệp đính kèm </label>
        <?= $this->Form->file('attachment', ['class' => 'form-control']) ?>
        <button type="submit" class="btn btn-success" style="width: 120px">Thêm Mới</button>
        <?= $this->Form->end() ?>
    </div>
</div>