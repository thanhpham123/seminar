<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Booking $booking
 */
?>
<div class="col-12 col-md-6 col-lg-4">
    <div class="single-rooms-area wow fadeInUp" data-wow-delay="100ms">
        <?= $this->Form->create($booking) ?>
        <fieldset>
            <legend><?= __('Edit Booking') ?></legend>
            <?php
                echo $this->Form->control('title',['class'=>'form-control']);
                echo $this->Form->control('description',['class'=>'form-control']);
                echo $this->Form->control('attachment',['class'=>'form-control']);
            ?>
        </fieldset>
        <button type="submit" class="btn btn-success" style="width: 120px">Sửa</button>
        <?= $this->Form->end() ?>
    </div>
</div>