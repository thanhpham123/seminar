<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Booking[]|\Cake\Collection\CollectionInterface $bookings
 */
use App\Model\Entity\User;
?>
<div class="row">

    <div class="col-md-10 col-md-push-2">
        <h2><?= __('Bookings') ?></h2>
        <a href="<?= $this->Url->build(['controller' => 'Bookings', 'action' => 'add']) ?>"
           class="btn btn-success btn-sm"> Thêm mới</a>


        <table cellpadding="0" cellspacing="0"  class="table table-striped">
            <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Người đăng') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($bookings as $booking): ?>
                <tr>
                    <td><?= $this->Number->format($booking->id) ?></td>

                    <td><?= h($booking->title) ?></td>
                    <td><?= h($booking->description) ?></td>
                    <td><?= $booking->has('user') ? $this->Html->link($booking->user->first_name, ['controller' => 'Users', 'action' => 'view', $booking->user->id]) : '' ?></td>
                    <td><?= h($booking->created) ?></td>
                    <td class="actions">
                        <a href="<?= $this->Url->build(['controller' => 'Bookings', 'action' => 'view', $booking->id]) ?>" class="btn btn-info btn-custom margin-bottom-cus" data-toggle="tooltip" title="Chi tiết">
                            <i class="fa fa-eye"></i>
                        </a>
                        <?php if($au==User::ROLE_ADMINISTRATOR ){?>
                        <a href="<?= $this->Url->build(['controller' => 'Bookings', 'action' => 'edit', $booking->id]) ?>" class="btn btn-warning btn-custom" data-toggle="tooltip" title="Sửa">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', array('class' => 'fa fa-trash')),
                            ['action' => 'delete', $booking->id],
                            [
                                'escape' => false,
                                'class' => 'btn btn-danger btn-custom',
                                'data-toggle' => 'tooltip',
                                'title' => 'Xoá',
                                'confirm' => __('Bạn có muốn Xoá Bài này #{0}?', $booking->id)
                            ]
                        ) ?>
                        <?php }?>

                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>

</div>